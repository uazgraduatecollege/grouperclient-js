'use strict'

const _ = require('lodash')
const fs = require('fs')
const request = require('request')
const sprintf = require('sprintf-js').sprintf

/**
 * Creates a new GrouperClient object
 * @class GrouperClient
 * @param {Object} params - An object containing constructor parameters
 * @param {String} params.caCertFile - Path to a CA cert file. Optional.
 * @param {String} params.domain - Server domain of Grouper host. Should include
 *   the URL protocol (eg. https://grouper.my.edu)
 * @param {String} params.user - Grouper client username
 * @param {String} params.password - Grouper client password
 * @param {String} params.apiRoot - API root path
 * @param {Integer} params.timeout - Request timeout in milliseconds.
 *   Default 20 seconds
 * @param {String} params.membersTpt - Request string template to query group
 *    memberships
 * @param {String} params.membershipsTpt - Request string template to query a
 *    subject's memberships
 * @param {String} params.modifyTpt - Add or remove member request string
  *   template
 */
function GrouperClient (params) {
  if (params.caCertFile) {
    this.caCertFile = fs.readFileSync(params.caCertFile)
  } else {
    this.caCertFile = null
  }
  this.domain = params.domain || null
  this.user = params.user || null
  this.password = params.password || null
  this.apiRoot = params.apiRoot || null
  this.timeout = params.timeout || 30000
  this.membersTpt = params.membersTpt || '/groups/%1$s/members'
  this.membershipsTpt = params.membershipsTpt || '/subjects/%1$s/memberships'
  this.modifyTpt = params.modifyTpt || '/groups/%1$s/members/%2$s'
}

/**
 * Query members of a group
 *
 * @param {String} groupId - The full path ID of a Grouper group
 * @param {Function} cb - Callback function
 */
GrouperClient.prototype.getMembers = function (groupId, cb) {
  request({
    url: `${this.domain}${this.apiRoot}${sprintf(this.membersTpt, groupId)}`,
    method: 'GET',
    timeout: this.timeout,
    auth: {
      user: this.user,
      pass: this.password
    },
    ca: this.caCertFile
  }, (error, response, body) => {
    if (error) return cb(error)

    if (response.statusCode !== 200) {
      return cb(new Error(`Error ${response.statusCode}: ${body}`))
    }

    const wsSubjects = JSON.parse(body).WsGetMembersLiteResult.wsSubjects
    const members = _.map(wsSubjects, 'id')
    cb(null, {
      statusCode: response.statusCode,
      members: members
    })
  })
}

/**
 * Query whether group has a member
 *
 * @param {String} groupId - The full path ID of a Grouper group
 * @param {String} subjectId - The subject ID
 * @param {Function} cb - Callback function
 */
GrouperClient.prototype.hasMember = function (groupId, subjectId, cb) {
  const membersUrl = `${this.domain}${this.apiRoot}` +
    sprintf(this.modifyTpt, groupId, subjectId)
  request({
    url: membersUrl,
    method: 'GET',
    timeout: this.timeout,
    auth: {
      user: this.user,
      pass: this.password
    },
    ca: this.caCertFile
  }, (error, response, body) => {
    if (error) return cb(error)

    if (response.statusCode !== 200) {
      return cb(new Error(`Error ${response.statusCode}: ${body}`))
    }

    let isMember = false
    const resultCode =
      JSON
      .parse(body)
      .WsHasMemberLiteResult
      .resultMetadata
      .resultCode

    if (resultCode === 'IS_MEMBER') isMember = true

    cb(null, {
      statusCode: response.statusCode,
      isMember: isMember
    })
  })
}

/**
 * Query a user's memberships
 *
 * The passed callback will be given an HTTP error or an object.
 *
 * @example
 * {
 *    statusCode: 200,
 *    memberships: ['one', 'two', 'three']
 * }
 *
 * @param {String} subjectId - The subjects's ID
 * @param {Function} cb - Callback that outputs a javscript object with
 *  properties 'statusCode' and 'memberships'
 */
GrouperClient.prototype.getMemberships = function (subjectId, cb) {
  const membershipsUrl = `${this.domain}${this.apiRoot}` +
    sprintf(this.membershipsTpt, subjectId)
  request({
    url: membershipsUrl,
    method: 'GET',
    timeout: this.timeout,
    auth: {
      user: this.user,
      pass: this.password
    },
    ca: this.caCertFile
  }, (error, response, body) => {
    if (error) return cb(error)

    if (response.statusCode !== 200) {
      return cb(new Error(`Error ${response.statusCode}: ${body}`))
    }

    const wsMemberships = JSON.parse(body).WsGetMembershipsResults.wsMemberships
    const memberships = _.map(wsMemberships, 'groupName')
    cb(null, {
      statusCode: response.statusCode,
      memberships: memberships
    })
  })
}

/**
 * Add a user to a group
 *
 * @param {String} groupId - The full path ID of the Grouper group
 * @param {String} subjectId - The subject's ID
 * @param {Function} cb - Callback function
 */
GrouperClient.prototype.add = function (groupId, subjectId, cb) {
  const addUrl = `${this.domain}${this.apiRoot}` +
    sprintf(this.modifyTpt, groupId, subjectId)
  request({
    url: addUrl,
    method: 'PUT',
    timeout: this.timeout,
    auth: {
      user: this.user,
      pass: this.password
    },
    ca: this.caCertFile
  }, (error, response, body) => {
    if (error) return cb(error)

    if (response.statusCode === 201) {
      cb(null, {
        statusCode: response.statusCode,
        message: 'SUCCESS: Member added',
        success: true
      })
    } else if (response.statusCode === 200) {
      cb(null, {
        statusCode: response.statusCode,
        message: 'SUCCESS: Member already existed',
        success: true
      })
    } else {
      return cb(new Error(`Error ${response.statusCode}: ${body}`))
    }
  })
}

/**
 * Remove a user from a group
 *
 * @param {String} groupId - The full path ID of the Grouper group
 * @param {String} subjectId - The subject's ID
 * @param {Function} cb - Callback function
 */
GrouperClient.prototype.remove = function (groupId, subjectId, cb) {
  const addUrl = `${this.domain}${this.apiRoot}` +
    sprintf(this.modifyTpt, groupId, subjectId)

  this.hasMember(groupId, subjectId, (err, res) => {
    if (err) return cb(err)

    if (res.isMember) {
      request({
        url: addUrl,
        method: 'DELETE',
        timeout: this.timeout,
        auth: {
          user: this.user,
          pass: this.password
        },
        ca: this.caCertFile
      }, (error, response, body) => {
        if (error) return cb(error)

        if (response.statusCode === 200) {
          cb(null, {
            statusCode: response.statusCode,
            message: 'SUCCESS: Member removed',
            success: true
          })
        } else {
          return cb(new Error(`Error ${response.statusCode}: ${body}`))
        }
      })
    } else {
      cb(null, {
        statusCode: null,
        message: 'SUCCESS: Was not a member',
        success: true
      })
    }
  })
}

module.exports = GrouperClient
