'use strict'
/* eslint-env mocha */
const expect = require('chai').expect
const GrouperClient = require('../index')

let testServer
let myClient = new GrouperClient({
  domain: 'http://localhost:3000',
  user: 'arthur',
  password: 'oneTwoFourNoThree!!',
  apiRoot: '/grouper-ws/servicesRest/json/v2_1_005'
})

const testApp = require('./testapp')(myClient)

describe('Grouper Client', () => {
  before((done) => {
    testServer = testApp.listen(3000, () => {
      done()
    })
  })

  after((done) => {
    testServer.close(() => {
      done()
    })
  })

  beforeEach(() => {
    myClient.domain = 'http://localhost:3000'
    myClient.apiRoot = '/grouper-ws/servicesRest/json/v2_1_005'
    myClient.timeout = 30000
  })

  describe('Verify test server', () => {
    it('does not error and returns a response', (done) => {
      myClient.getMembers('camel.ot:round-table:knights', (error, response) => {
        expect(error).to.be.null
        expect(response).to.be.ok
        done()
      })
    })
  })

  describe('A GrouperClient with invalid server settings', () => {
    context('with an invalid domain', () => {
      it('should receive an error', (done) => {
        myClient.domain = 'https://frenchcastle:3000'
        myClient.timeout = 1 // force premature timeout
        myClient.getMembers('camel.ot:round-table:knights', (error, response) => {
          expect(error).to.be.an('error')
          done()
        })
      })
    })
    context('with an invalid apiRoot', () => {
      it('should receive an error', (done) => {
        myClient.apiRoot = '/french/castle'
        myClient.getMembers('camel.ot:round-table:knights', (error, response) => {
          expect(error).to.be.an('error')
          done()
        })
      })
    })
  })

  describe('GrouperClient#getMembers', () => {
    context('with a valid group identifier', () => {
      it('should return an array of subject IDs', (done) => {
        myClient.getMembers('camel.ot:round-table:knights', (error, response) => {
          expect(error).to.be.null
          expect(response).to.be.ok
          expect(response).to.have.property('statusCode')
          expect(response).to.have.property('members')
          expect(response.members).to.be.an('array')
          done()
        })
      })
    })
    context('with an invalid group identifier', () => {
      it('should receive an error', (done) => {
        myClient.getMembers('castle.fr:table-ronde:knights', (error, response) => {
          expect(error).to.be.an('error')
          done()
        })
      })
    })
  })

  describe('GrouperClient#getMemberships', () => {
    context('with a valid subject identifier', () => {
      it('should return an array of group IDs', (done) => {
        myClient.getMemberships('lancelot', (error, response) => {
          expect(error).to.be.null
          expect(response).to.be.ok
          expect(response).to.have.property('statusCode')
          expect(response).to.have.property('memberships')
          expect(response.memberships).to.be.an('array')
          done()
        })
      })
    })
    context('with an invalid subject identifier', () => {
      it('should receive an error', (done) => {
        myClient.getMemberships('zootsGrail', (error, response) => {
          expect(error).to.be.an('error')
          done()
        })
      })
    })
  })

  describe('GrouperClient#hasMember', () => {
    context('with valid group & subject IDs', () => {
      it('should return an object indicating true or false', (done) => {
        myClient.hasMember('camel.ot:round-table:knights', 'robin', (error, response) => {
          expect(error).to.be.null
          expect(response).to.be.an('object')
          expect(response).to.have.property('isMember')
          expect(response.isMember).to.be.a('boolean')
          done()
        })
      })
    })
  })

  describe('GrouperClient#add', () => {
    context('with valid group & subject IDs', () => {
      it('should return an object indicating success or failure', (done) => {
        myClient.add('camel.ot:round-table:knights', 'patsy', (error, response) => {
          expect(error).to.be.null
          expect(response).to.be.an('object')
          expect(response).to.have.property('statusCode')
          expect(response).to.have.property('message')
          expect(response).to.have.property('success')
          expect(response.success).to.be.a('boolean')
          done()
        })
      })
    })
  })

  describe('GrouperClient#remove', () => {
    context('with valid group & subject IDs', () => {
      it('should return an object indicating success or failure', (done) => {
        myClient.remove('camel.ot:round-table:knights', 'patsy', (error, response) => {
          expect(error).to.be.null
          expect(response).to.be.an('object')
          expect(response).to.have.property('statusCode')
          expect(response).to.have.property('message')
          expect(response).to.have.property('success')
          expect(response.success).to.be.a('boolean')
          done()
        })
      })
    })
  })
})
