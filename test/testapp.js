'use strict'

// dummy getMemberships() response
const subjectMembershipsBody = {
  WsGetMembershipsResults: {
    responseMetadata: { millis: '249', serverVersion: '2.2.1' },
    resultMetadata: {
      resultCode: 'SUCCESS',
      resultMessage: 'Found results involving 1 groups and 1 subjects',
      success: 'T'
    },
    wsGroups: [
      {
        description: 'Knights of the Round Table eat ham and jam and spam a lot',
        displayExtension: 'knights',
        displayName: 'camel.ot:RoundTable:knights',
        extension: 'knights',
        name: 'camel.ot:round-table:knights',
        typeOfGroup: 'group',
        uuid: '93fea9c4ba7c416da702f0936365ce91'
      }
    ],
    wsMemberships: [
      {
        createTime: '2016/04/14 16:16:58.877',
        enabled: 'T',
        groupId: '93fea9c4ba7c416da702f0936365ce91',
        groupName: 'camel.ot:round-table:knights',
        immediateMembershipId: '5e757122646e432ab1d9eb174bb2f440',
        listName: 'members',
        listType: 'list',
        memberId: '075eade9d1b0474ca2f985de20dc32ab',
        membershipId: '5e757121646e432ab1d9eb074bb2f340:93a12a5ce6a545ec85a94b79587f4423',
        membershipType: 'immediate',
        subjectId: 'lancelot',
        subjectSourceId: 'ldap'
      }
    ],
    wsSubjects: [
      {
        id: '',
        resultCode: 'SUCCESS',
        sourceId: 'ldap',
        success: 'T'
      }
    ]
  }
}

// dummy getMembers() response
const groupMembersBody = {
  WsGetMembersLiteResult: {
    responseMetadata: { millis: '68', serverVersion: '2.2.1' },
    resultMetadata: {
      resultCode: 'SUCCESS',
      resultMessage: 'Success for: clientVersion: 2.1.5, wsGroupLookups: Array size: 1: [0]: WsGroupLookup[pitGroups=[],groupName=camel.ot:round-table:knights]\n\n, memberFilter: All, includeSubjectDetail: false, actAsSubject: null, fieldName: null, subjectAttributeNames: null\n, paramNames: \n, params: null\n, sourceIds: null\n, pointInTimeFrom: null, pointInTimeTo: null, pageSize: null, pageNumber: null, sortString: null, ascending: null',
      success: 'T'
    },
    wsGroup: {
      description: 'Knights of the Round Table eat ham and jam and spam a lot',
      displayExtension: 'knights',
      displayName: 'camel.ot:RoundTable:knights',
      extension: 'knights',
      name: 'camel.ot:round-table:knights',
      typeOfGroup: 'group',
      uuid: '93fea9c4ba7c416da702f0936365ce91'
    },
    wsSubjects: [
      {
        id: 'arthur',
        resultCode: 'SUCCESS',
        sourceId: 'ldap',
        success: 'T'
      },
      {
        id: 'bedevere',
        resultCode: 'SUCCESS',
        sourceId: 'ldap',
        success: 'T'
      },
      {
        id: 'robin',
        resultCode: 'SUCCESS',
        sourceId: 'ldap',
        success: 'T'
      },
      {
        id: 'lancelot',
        resultCode: 'SUCCESS',
        sourceId: 'ldap',
        success: 'T'
      },
      {
        id: 'galahad',
        resultCode: 'SUCCESS',
        sourceId: 'ldap',
        success: 'T'
      }
    ]
  }
}

// dummy hasMember() response
const hasMemberBody = {
  WsHasMemberLiteResult: {
    responseMetadata: { millis: '69', serverVersion: '2.2.1' },
    resultMetadata: {
      resultCode: 'IS_MEMBER',
      resultMessage: 'Success for: clientVersion: 2.1.5, wsGroupLookup: WsGroupLookup[pitGroups=[],groupName=camel.ot:round-table:knights], subjectLookups: Array size: 1: [0]: WsSubjectLookup[subjectId=robin]\n\n memberFilter: All, actAsSubject: null, fieldName: null, includeGroupDetail: false, includeSubjectDetail: false, subjectAttributeNames: null\n,params: null\n,pointInTimeFrom: null, pointInTimeTo: null',
      success: 'T'
    },
    wsGroup: {
      description: 'Knights of the Round Table eat ham and jam and spam a lot',
      displayExtension: 'knights',
      displayName: 'camel.ot:RoundTable:knights',
      extension: 'knights',
      name: 'camel.ot:round-table:knights',
      typeOfGroup: 'group',
      uuid: '93fea9c4ba7c416da702f0936365ce91'
    },
    wsSubject: {
      id: 'robin',
      name: 'Sir Robin the Brave',
      resultCode: 'SUCCESS',
      sourceId: 'ldap',
      success: 'T'
    }
  }
}

// dummy add() response
const addSuccessBody = {
  WsAddMemberLiteResult: {
    responseMetadata: { millis: '190', serverVersion: '2.2.1' },
    resultMetadata: {
      resultCode: 'SUCCESS',
      resultMessage: 'Success for: clientVersion: 2.1.5, wsGroupLookup: WsGroupLookup[pitGroups=[],groupName=camel.ot:round-table:squires], subjectLookups: Array size: 1: [0]: WsSubjectLookup[subjectId=patsy]\n\n, replaceAllExisting: false, actAsSubject: null, fieldName: null, txType: NONE, includeGroupDetail: false, includeSubjectDetail: false, subjectAttributeNames: null\n, params: null\n, disabledDate: null, enabledDate: null',
      success: 'T'
    },
    wsGroupAssigned: {
      description: 'Squires of the Round Table bang coconuts together',
      displayExtension: 'squires',
      displayName: 'camel.ot:RoundTable:Squires',
      extension: 'squires',
      name: 'camel.ot:round-table:squires',
      typeOfGroup: 'group',
      uuid: '51868d80f3384e7d935cb3665ab77685'
    },
    wsSubject: {
      id: 'patsy',
      name: 'Patsy the Squire',
      resultCode: 'SUCCESS',
      sourceId: 'ldap',
      success: 'T'
    }
  }
}

// dummy remove response
const removeSuccessBody = {
  WsDeleteMemberLiteResult: {
    responseMetadata: { millis: '190', serverVersion: '2.2.1' },
    resultMetadata: {
      resultCode: 'SUCCESS',
      resultMessage: 'Success for: clientVersion: 2.1.5, wsGroupLookup: WsGroupLookup[pitGroups=[],groupName=camel.ot:round-table:squires], subjectLookups: Array size: 1: [0]: WsSubjectLookup[subjectId=patsy]\n\n, actAsSubject: null, fieldName: null, txType: NONE\n, params: null',
      success: 'T'
    },
    wsGroup: {
      description: 'Squires of the Round Table bang coconuts together',
      displayExtension: 'squires',
      displayName: 'camel.ot:RoundTable:Squires',
      extension: 'squires',
      name: 'camel.ot:round-table:squires',
      typeOfGroup: 'group',
      uuid: '51868d80f3384e8d935bb3665ab77685'
    },
    wsSubject: {
      id: 'patsy',
      name: 'Patsy the Squire',
      resultCode: 'SUCCESS',
      sourceId: 'ldap',
      success: 'T'
    }
  }
}

module.exports = function (myClient) {
  let testApp = require('express')()

  // get members
  testApp.get(`${myClient.apiRoot}/groups/:group/members`, (req, res) => {
    if (req.params.group !== 'camel.ot:round-table:knights') {
      res
      .status(404)
      .send('Not found')
    } else {
      res
      .status(200)
      .send(groupMembersBody)
    }
  })

  // has member
  testApp.get(`${myClient.apiRoot}/groups/:group/members/:subject`, (req, res) => {
    if (req.params.group !== 'camel.ot:round-table:knights') {
      res
      .status(404)
      .send('Not found')
    } else {
      res
      .status(200)
      .send(hasMemberBody)
    }
  })

  // get memberships
  testApp.get(`${myClient.apiRoot}/subjects/:subject/memberships`, (req, res) => {
    if (req.params.subject !== 'lancelot') {
      res
      .status(404)
      .send('Not found')
    } else {
      res
      .status(200)
      .send(subjectMembershipsBody)
    }
  })

  // add member
  testApp.put(`${myClient.apiRoot}/groups/:group/members/:subject`, (req, res) => {
    res
    .status(200)
    .send(addSuccessBody)
  })

  // remove member
  testApp.delete(`${myClient.apiRoot}/groups/:group/members/:subject`, (req, res) => {
    res
    .status(200)
    .send(removeSuccessBody)
  })

  return testApp
}
