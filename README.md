# GrouperClient

A NodeJS lib to query and manage Grouper group members and memberships

## Requirements

Use of GrouperClient assumes a working [NodeJS](https://nodejs.org/) and [NPM](https://www.npmjs.com/) environment.
GrouperClient has been tested successfully with NodeJS versions 6.6+ and 8.9+

## Installation

### Direct installation from Git
1. Clone the git repository
2. Install dependencies

```sh
$ git clone <git-repo-url> <output-dir>
$ cd <output-dir>
$ npm install
```

### Installation as an NPM Dependency
```sh
$ cd <my_node_project>
$ npm install --save git+https://bitbucket.org:uazgraduatecollege/grouperclient-js.git
```

## Usage

### Initialize a GrouperClient object:

```javascript
// The first 4 keys are required, the others are shown with default values
var myClient = new GrouperClient({
  domain: 'http://localhost:3000', // required. The protocol & domain of the Grouper web API. Port can be specified optionally.
  user: 'arthur_king', // required. The username of your Grouper client credentials.
  password: 'oneTwoFourNoThree!!', // required. The password of your Grouper client credentials.
  apiRoot: '/path/to/grouper/json', // required. The web API path on the Grouper domain.
  timeout: "10000", // options. Milliseconds before the client request will timeout.
  membersTpt: '/groups/%1$s/members', // optional. The path template of the `members` API call.
  membershipsTpt: '/subjects/%1$s/memberships', // optional. The path template of the `memberships` API call.
  modifyTpt: '/groups/%1$s/members/%2$s', // optional. The API path template for adding & removing group memberships.
  caCertFile: null // optional, local filesystem path to the API host's CA file.
})
```

### GrouperClient.getMembers(groupId)

Query members of a group

```javascript
myClient.getMembers('camel.ot:round-table:knights', (err, res) => {
  if (err) {
    console.error(err)
  } else {
    console.log(res)
  }
})
```

Successful output[^1]:

```javascript
{ statusCode: 200,
  members: [ 'arthur', 'bedevere', 'robin', 'lancelot', 'galahad' ] }
```

[^1] Actually, `members` will likely be an array of numeric subject identifiers, but for README purposes, these are more fun.

### GrouperClient.hasMember(groupId, subjectId)

See whether a group has a given members [^2]

```javascript
myClient.hasMember(
  'camel.ot:round-table:knights', 'patsy', (err, res) => {
    if (err) {
      console.error(err)
    } else {
      console.log(res)
    }
  }
)
```

[^2] As with `getMembers()`, `subjectId` will likely need to be a numeric subject ID.

Successful response:

```javascript
{ statusCode: 200, isMember: true }
```

### GrouperClient.getMemberships(subjectId)

Query membership of a given subject

```javascript
myClient.getMemberships('lancelot', (err, res) => {
  if (err) {
    console.error(err)
  } else {
    console.log(res)
  }
})
```

Successful response:

```javascript
{ statusCode: 200,
  memberships: [ 'camel.ot:round-table:knights' ] }
```

### GrouperClient.add(groupId, subjectId)

Add a subject to a group

```javascript
myClient.add('camel.ot:round-table:squires', 'patsy', (err, res) => {
  if (err) {
    console.error(err)
  } else {
    console.log(res)
  }
})
```

Successful response:

```javascript
{ statusCode: 200,
  message: 'SUCCESS: Member already existed',
  success: true }
```

### GrouperClient.remove(subjectId, groupId)

Remove a subject from a group

```javascript
myClient.remove('camel.ot:round-table:squires', 'patsy', (err, res) => {
  if (err) {
    console.error(err)
  } else {
    console.log(res)
  }
})
```

Successful response:

```javascript
{ statusCode: 200,
  message: 'SUCCESS: Member removed',
  success: true }
```

## License

Copyright (c) 2017 Arizona Board of Regents on behalf of the University of Arizona, all rights reserved


## Status

A work in-progress, may be `unstable`. Contributors welcome if you bring us a shrubbery.
